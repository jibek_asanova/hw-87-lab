import React, {useEffect} from 'react';
import {Redirect} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {createComment, fetchComments} from "../../store/actions/commentsActions";
import {fetchPost} from "../../store/actions/postsActions";
import {Box, Grid, Paper, Typography} from "@material-ui/core";
import CommentItem from "../../components/CommentItem/CommentItem";
import CommentForm from "../../components/CommentForm/CommentForm";

const Post = ({match}) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.post);
    const comments = useSelector(state => state.comments.comments);
    const user = useSelector(state => state.users.user);


    useEffect(() => {
        dispatch(fetchPost(match.params.id))
    }, [dispatch, match.params.id]);

    useEffect(() => {
        dispatch(fetchComments(match.params.id));
    }, [dispatch, match.params.id]);

    const onSubmit = async (commentData) => {
        commentData.post = match.params.id;
        await dispatch(createComment(commentData, match.params.id));
    };
    if(!user) {
        return <Redirect to="/login"/>
    }


    return post && (
        <Grid container direction="column" spacing={2}>
            <Paper component={Box} p={2}>
                <Typography variant="h4">{post.title} by {post.user.username}</Typography>
                <Typography variant="subtitle1">At {post.datetime}</Typography>
                <Typography variant="body1">{post.description}</Typography>
            </Paper>
            <Grid item>
                <Typography variant="h4">Comments</Typography>
            </Grid>
            <Grid item container direction="column" spacing={1}>
                {comments.map(comment => (
                    <CommentItem
                        key={comment._id}
                        user={comment.user.username}
                        comment={comment.description}
                    />
                ))}
            </Grid>
            <Grid item>
                <Typography variant="h4">Create Comment</Typography>
            </Grid>
            <CommentForm
                onSubmit={onSubmit}
            />
        </Grid>
    );
};

export default Post;