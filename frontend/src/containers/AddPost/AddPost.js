import React, {useState} from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {createPost} from "../../store/actions/postsActions";
import {Alert} from "@material-ui/lab";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%"
    }
}));
const AddPost = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.posts.error);

    const [state, setState] = useState({
        title: "",
        description: "",
        image: null,
    });

    const submitFormHandler = async e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        await dispatch(createPost(formData));
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    }

    return (
        <>
            <Typography variant="h4">Add new post</Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
            >
                {error &&
                <Alert severity="error" className={classes.alert}>
                    {error.error || error.global}
                </Alert>
                }
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Title"
                        name="title"
                        value={state.title}
                        onChange={inputChangeHandler}
                        required
                    />
                </Grid>

                <Grid item xs>
                    <TextField
                        fullWidth
                        multiline
                        rows={3}
                        variant="outlined"
                        label="Description"
                        name="description"
                        value={state.description}
                        onChange={inputChangeHandler}
                        helperText={error && <span>{error.error}</span>}
                    />
                </Grid>

                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Create</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default AddPost;