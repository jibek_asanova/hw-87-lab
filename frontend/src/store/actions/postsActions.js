import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_FAILURE = 'CREATE_POST_FAILURE';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, payload: posts});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, payload: post});
export const fetchPostFailure = () => ({type: FETCH_POST_FAILURE});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = error => ({type: CREATE_POST_FAILURE, payload: error});

export const fetchPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axiosApi.get('/posts');
            dispatch(fetchPostsSuccess(response.data));
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(fetchPostsFailure(error.response.data));
                toast.error(`error: ${error.response.data.error}`);
            } else {
                dispatch(fetchPostsFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    }
};

export const fetchPost = id => {
    return async dispatch => {
        try {
            dispatch(fetchPostRequest());
            const response = await axiosApi.get('/posts/' + id);
            dispatch(fetchPostSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostFailure());
        }
    };
};

export const createPost = postData => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(createPostRequest());
            await axiosApi.post('/posts', postData, {headers});
            dispatch(createPostSuccess(postData));
            toast.success('Post Created!');
            dispatch(historyPush('/'));
        } catch (error) {
            if(error.response.status === 401) {
                toast.warning('You need to login')
            } else if(error.response && error.response.data) {
                dispatch(createPostFailure(error.response.data));
            } else {
                dispatch(createPostFailure({global: 'No internet'}));
                toast.error('No internet');
            }
        }
    };
};