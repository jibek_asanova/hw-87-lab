import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';

export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_FAILURE = 'CREATE_COMMENT_FAILURE';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, payload: comments});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const createCommentRequest = () => ({type: CREATE_COMMENT_REQUEST});
export const createCommentSuccess = () => ({type: CREATE_COMMENT_SUCCESS});
export const createCommentFailure = () => ({type: CREATE_COMMENT_FAILURE});

export const fetchComments = (id) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(fetchCommentsRequest());
            const response = await axiosApi.get('/comments/' + id, {headers});
            console.log(response.data)
            dispatch(fetchCommentsSuccess(response.data));
        } catch (error) {
            if(error.response.status === 401) {
                toast.warning('You need to login')
            } else {
                toast.error('Could not fetch comments!', {
                    theme: 'colored',
                })
            }
            dispatch(fetchCommentsFailure());
        }
    };
};

export const createComment = (commentData, post) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization' : getState().users.user && getState().users.user.token
            };
            dispatch(createCommentRequest());
            await axiosApi.post('/comments', commentData, {headers});
            dispatch(fetchComments(post));
            dispatch(createCommentSuccess());
        } catch (e) {
            dispatch(createCommentFailure());
            throw e;
        }
    };
};