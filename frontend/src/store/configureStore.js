import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import usersReducer from "./reducers/usersReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import postsReducer from "./reducers/postsReducer";
import commentsReducer from "./reducers/commentsReducer";

const rootReducer = combineReducers({
    'users': usersReducer,
    'posts': postsReducer,
    'comments': commentsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk)
    ));

store.subscribe(() => {
    saveToLocalStorage({
        users: store.getState().users,
    });
});



export default store;