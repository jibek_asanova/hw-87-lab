import {
    Box, Paper,
    Typography
} from "@material-ui/core";

import React from "react";


const CommentItem = ({user, comment}) => {

    return (
        <Paper component={Box} p={2}>
            <Typography variant="h4">Author: {user}</Typography>
            <Typography variant="body1">Comment: {comment}</Typography>
        </Paper>
    );
};

export default CommentItem;