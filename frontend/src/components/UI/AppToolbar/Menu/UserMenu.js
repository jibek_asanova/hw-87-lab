import React from 'react';
import {Button, Grid} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import {Link} from "react-router-dom";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();

    return (
        <>
            <Grid item>
                <Button aria-controls="simple-menu" aria-haspopup="true" color="inherit">
                Hello, {user.username}!
            </Button>
                <Button color="inherit" component={Link} to="/posts/new">Add new post</Button>
                or
                <Button color="inherit" onClick={() => dispatch(logoutUser())}>Logout</Button>
            </Grid>
        </>
    );
};

export default UserMenu;