import {
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ForumIcon from '@material-ui/icons/Forum';
import {apiURL} from "../../config";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
    card: {
        height: '100%',
    },
    media: {
        height: 0,
        paddingTop: '56.25%' // 16:9
    }
});

const PostItem = ({id, title, username, dateTime, image, description}) => {
    const classes = useStyles();
    const cardImage = apiURL + '/' + image;

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                {image ? <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                /> : <ForumIcon/>}
                <CardContent>
                    <Typography variant="subtitle1">
                        Time: {dateTime} by {username}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Typography variant="subtitle1">
                        {description}
                    </Typography>
                </CardActions>
                <CardActions>
                    <IconButton component={Link} to={'/posts/' + id}>
                        <ArrowForwardIcon/>
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default PostItem;