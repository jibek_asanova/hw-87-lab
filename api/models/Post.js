const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  datetime: {
    type: Date,
    default: Date.now()
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  description: String,
  image: String,
});

PostSchema.plugin(idvalidator);

const Post = mongoose.model('Post', PostSchema);
module.exports = Post;