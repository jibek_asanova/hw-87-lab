const express = require("express");
const Comment = require("../models/Comment");
const auth = require("../middleware/auth");
const router = express.Router();

router.get('/:id', auth, async (req, res) => {
  try {
    const comments = await Comment.find({post: req.params.id})
      .populate('user', 'username -_id')
    ;
    res.send(comments);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, async (req, res) => {
  if (!req.body.description || !req.body.post) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const commentData = {
    user: req.user._id,
    description: req.body.description,
    post: req.body.post
  };

  const comment = new Comment(commentData);
  try {
    await comment.save();
    res.send(comment);
  } catch {
    res.status(400).send({error: 'Data not valid, something error'});
  }
});



module.exports = router;