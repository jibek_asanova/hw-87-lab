const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Post = require('../models/Post');
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', auth, upload.single('image'), async (req, res) => {
  if (!req.body.title) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const postData = {
    title: req.body.title,
    user: req.user,
  };


  if (req.file) {
    postData.image = 'uploads/' + req.file.filename;
  }

  if(req.body.description) {
    postData.description = req.body.description;
  }

  if(!req.body.description && !req.file) {
    return res.status(500).send({error: 'Description field or image field should not be empty!'});
  }

  const post = new Post(postData);
  try {
    await post.save();
    res.send(post);
  } catch {
    res.status(400).send({error: 'Data not valid, something error'});
  }
});

router.get('/',  async (req, res) => {
  try {
    const products = await Post.find()
      .populate('user', 'username')
      .sort({datetime: -1})
    ;
    res.send(products);
  } catch (e) {
    res.sendStatus(500);
  }
});
router.get('/:id', async (req, res) => {
  try {
    const post = await Post.findById(req.params.id).populate('user', 'username');
    if (post) {
      res.send(post);
    } else {
      res.sendStatus(404).send({error: 'Post not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

module.exports = router;