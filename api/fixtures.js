const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Post = require('./models/Post');
const Comment = require('./models/Comment');

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2] = await User.create({
      username: 'admin',
      password: '123',
      token: nanoid(),
    }, {
      username: 'root',
      password: '123',
      token: nanoid(),
    }
  )

  const [post1, post2] = await Post.create({
      title: 'Post #1',
      user: user1,
      description: 'test',
      image: 'fixtures/rihana.jpg',
    }, {
      title: 'Post #2',
      user: user2,
      description: 'test',
      image: 'fixtures/taylorSwift.png',
    },
    {
      title: 'Post #3',
      user: user2,
      description: 'test',
      image: 'fixtures/taylorSwift.png',
    });

  await Comment.create({
      description: 'comment1',
      user: user1,
      post: post1
    }, {
      description: 'comment2',
      user: user1,
      post: post1
    },
    {
      description: 'comment1',
      user: user1,
      post: post2
    }
  )


  await mongoose.connection.close();
};

run().catch(console.error)